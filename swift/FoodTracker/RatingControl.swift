//
//  RatingControl.swift
//  FoodTracker
//
//  Created by Mattias on 15/04/2017.
//  Copyright © 2017 Mattias. All rights reserved.
//

import UIKit

@IBDesignable class RatingControl: UIStackView {
    
    //MARK: properties
    private var ratingButtons = [UIButton]()
    var rating = 0 {
        didSet{ updateBUttonSelectionState() }
    }
    
    // inspectable means that you can change this in the attributes inspection window
    @IBInspectable var starSize: CGSize = CGSize(width: 44, height: 44){
        didSet{setupButtons()}
    }
    
    @IBInspectable var starCount: Int = 5 {
        didSet{setupButtons()}
    }
    
    //MARK: init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupButtons()
    }
    required init(coder: NSCoder) {
        super.init(coder: coder)
        setupButtons()
    }
    
    //MARK: private methods
    private func setupButtons(){
        // clear btns
        for button in ratingButtons{
            removeArrangedSubview(button)
            button.removeFromSuperview()
        }
        
        ratingButtons.removeAll()
        
        // load images
        let bundle = Bundle(for: type(of: self))
        let filledStar = UIImage(named: "filledStar", in: bundle, compatibleWith: self.traitCollection)
        let emptyStar = UIImage(named:"emptyStar", in: bundle, compatibleWith: self.traitCollection)
        let highlightedStar = UIImage(named:"highlightedStar", in: bundle, compatibleWith: self.traitCollection)
        
        for index in 0..<starCount{
            // create
            let button = UIButton();
        
            // setup correct image on state
            button.setImage(emptyStar, for: .normal)
            button.setImage(filledStar, for: .selected)
            button.setImage(highlightedStar, for: .highlighted)
            button.setImage(highlightedStar, for: [.highlighted, .selected])
        
            // disable automatically generated constraints
            button.translatesAutoresizingMaskIntoConstraints = false
            
            // define own
            button.heightAnchor.constraint(equalToConstant: starSize.height).isActive = true
            button.widthAnchor.constraint(equalToConstant: starSize.width).isActive = true
            
            // set up accessibility
            button.accessibilityLabel = "Set \(index + 1) start rating"
            
            //hook up to action
            button.addTarget(self, action: #selector(RatingControl.ratingButtonTapped(button:)), for: .touchUpInside)
            
            // add to stack
            addArrangedSubview(button)
            ratingButtons.append(button)
        }
    }
    
    private func updateBUttonSelectionState(){
        for(index, button) in ratingButtons.enumerated(){
            // selected => highlighted for those smaller than rating
            let hintString: String?
            if rating == index+1 {
                hintString = "Tap to reset the rating to zero."
            } else{
                hintString = nil
            }
            // Calculate the value string
            let valueString: String
            switch (rating) {
            case 0:
                valueString = "No rating set."
            case 1:
                valueString = "1 star set."
            default:
                valueString = "\(rating) stars set."
            }
            // Assign the hint string and value string
            button.accessibilityHint = hintString
            button.accessibilityValue = valueString
            button.isSelected = index < rating
        }
    }
    
    //MARK: button action
    func ratingButtonTapped(button: UIButton) {
        guard let index = ratingButtons.index(of: button) else{
            fatalError("The button is not in hte ratingbuttons array")
        }
        let selectedRating = index + 1
        if selectedRating == rating {
            // if tapping current rating reset
            rating = 0
        } else{
            rating = selectedRating
        }
    }
    
}
