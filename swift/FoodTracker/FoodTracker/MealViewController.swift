//
//  MealViewController
//  FoodTracker
//
//  Created by Mattias on 14/04/2017.
//  Copyright © 2017 Mattias. All rights reserved.
//

import UIKit
import os.log

class MealViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //MARK: Properties
    @IBOutlet weak var nameInput: UITextField!
    @IBOutlet weak var mealImage: UIImageView!
    @IBOutlet weak var ratingControl: RatingControl!
    @IBOutlet weak var saveMeal: UIBarButtonItem!
    @IBOutlet weak var cancel: UIBarButtonItem!
    
    // holder for creating a new meal
    var meal: Meal?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        // handle typing with a delegate callback
        nameInput.delegate = self
        
        // set up view with existing data received from previous view
        if let meal = meal {
            navigationItem.title = meal.name
            nameInput.text = meal.name
            mealImage.image = meal.photo
            ratingControl.rating = meal.rating
        }
        
        updateSaveButtonState()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK UITextFIelDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // hide the keyboard
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textfield: UITextField){
        saveMeal.isEnabled = false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateSaveButtonState()
        navigationItem.title = textField.text
    }
    
    private func updateSaveButtonState(){
        let text = nameInput.text ?? ""
        saveMeal.isEnabled = !text.isEmpty
    }
    
    //MARK: navigation
    // save button is tied to a segue = transition
    // before going we prepare by saving or canceling
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        super.prepare(for: segue, sender: sender)
        guard let button = sender as? UIBarButtonItem, button === saveMeal else{
            os_log("The save button was not pressed, cancellint", log: OSLog.default, type: .debug)
            return
        }
        let name = nameInput.text ?? ""
        let photo = mealImage.image
        let rating = ratingControl.rating
        meal = Meal(name: name, photo: photo, rating: rating)
    }
    
    //MARK: Actions
    @IBAction func selectPhotoFromLibrary(_ sender: UITapGestureRecognizer) {
        // hide keyboard
        nameInput.resignFirstResponder()
        // open image picker, only from library
        let imagePickercontoller = UIImagePickerController()
        imagePickercontoller.sourceType = .photoLibrary
        // bind to ViewController
        imagePickercontoller.delegate = self
        present(imagePickercontoller, animated: true, completion: nil)
    }
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        let isPresentingInAddMeal = presentingViewController is UINavigationController
        // when creating a new meal itsa modal
        if isPresentingInAddMeal{
            dismiss(animated: true, completion: nil)
        } else if let owningNavigationController = navigationController{
            //If the view controller has been pushed onto a navigation stack, this property contains a reference to the stack’s navigation controller.
            owningNavigationController.popViewController(animated: true)
        } else{
            fatalError("MEalVIewcontroller is not inside a navigation controller")
        }
    }
    
    //MARK: UIImagePicker delegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
            else{
                fatalError("Expected a dict conta imafe btut got shit: \(info)")
        }
        mealImage.image = selectedImage
        dismiss(animated: true, completion: nil)
    }
    
    
}

