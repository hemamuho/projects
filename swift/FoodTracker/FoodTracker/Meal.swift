//
//  Meal.swift
//  FoodTracker
//
//  Created by Mattias on 15/04/2017.
//  Copyright © 2017 Mattias. All rights reserved.
//

import UIKit
import os.log
class Meal: NSObject, NSCoding{
    
    //MARK: properties
    var name: String
    var photo: UIImage?
    var rating: Int
    
    //MARK: Types
    struct PropertyKey {
        static let name = "name"
        static let photo = "photo"
        static let rating = "rating"
    }
    
    //MARK: init
    init?(name: String, photo: UIImage?, rating: Int){
        
        // should fail when
        guard !name.isEmpty else{return nil}
        guard (rating>=0) && (rating<=5) else{return nil}
        
        // init
        self.name = name
        self.photo = photo
        self.rating = rating
    }
    
    
    //MARK: nscoding
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: PropertyKey.name)
        aCoder.encode(photo, forKey: PropertyKey.photo)
        aCoder.encode(rating, forKey: PropertyKey.rating)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        // name is required
        guard let name = aDecoder.decodeObject(forKey: PropertyKey.name) as? String else{
            os_log("unable to decode name for meal", log: OSLog.default, type: .debug)
            return nil
        }
        // image is optional so just conditinl cast and nil is fine
        let photo = aDecoder.decodeObject(forKey: PropertyKey.photo) as? UIImage
        let rating = aDecoder.decodeInteger(forKey: PropertyKey.rating)
        
        // must call designated initialiser i.e. self
        self.init(name: name ,photo: photo, rating: rating)
    }
    
    //MARK: Archiving paths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("meals")
    
}
