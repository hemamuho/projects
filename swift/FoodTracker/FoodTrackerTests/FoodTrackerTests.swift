//
//  FoodTrackerTests.swift
//  FoodTrackerTests
//
//  Created by Mattias on 14/04/2017.
//  Copyright © 2017 Mattias. All rights reserved.
//

import XCTest
@testable import FoodTracker

class FoodTrackerTests: XCTestCase {
    
    //MARK: Meal class test
    func testMealClassInitSuccess() {
        let zeroRated = FoodTracker.Meal.init(name: "Zero", photo: nil, rating: 0)
        XCTAssertNotNil(zeroRated)
        let fiveRated = FoodTracker.Meal.init(name: "Zero", photo: nil, rating: 5)
        XCTAssertNotNil(fiveRated)
    }
    
    func testMealInitFail(){
        let negativeRatedMeal = FoodTracker.Meal.init(name: "Zero", photo: nil, rating: -6)
        XCTAssertNil(negativeRatedMeal)
        let nonameMeal = FoodTracker.Meal.init(name: "", photo: nil, rating: 0)
        XCTAssertNil(nonameMeal)
        
        let largeRating = FoodTracker.Meal.init(name: "Large", photo: nil, rating: 600)
        XCTAssertNil(largeRating)
    }
    
}
