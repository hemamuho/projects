export function getDistanceBetweenPoints(station, point) {
  // distance = sqrt(x1-x2)^2 + (y2-y1)^2)
  const xD = station.x - point.x;
  const yD = station.y - point.y;
  const distance = Math.sqrt(xD * xD + yD * yD);
  return distance;
}

export function getPowerToPoint(station, point) {
  // power = (reach - distance)^2
  const p = station.r - getDistanceBetweenPoints(station, point);
  const power = p * p;
  return { ...station, power: power };
}

export function filterOutOfReach(stations, point) {
  const inReach = stations.filter(s => {
    return s.r > getDistanceBetweenPoints(s, point);
  });
  return inReach;
}

export function getBestLinkStation(stations, point) {
  const powers = filterOutOfReach(stations, point).map(s => {
    return getPowerToPoint(s, point);
  });
  const best = powers.reduce((best, curr) => {
    return best > curr ? best : curr;
  }, 0);
  return best !== 0 ? best : null;
}
