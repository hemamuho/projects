import React, { Component } from "react";
import { getBestLinkStation } from "./logic";

class App extends Component {
  constructor() {
    super();
    this.state = {
      stations: [
        { x: 0, y: 0, r: 10 },
        { x: 20, y: 20, r: 5 },
        { x: 10, y: 0, r: 12 }
      ],
      points: [
        { x: 0, y: 0 },
        { x: 100, y: 100 },
        { x: 15, y: 10 },
        { x: 18, y: 18 }
      ]
    };
  }

  getStringPresentation(stationWithPower, point) {
    if (stationWithPower) {
      return `Best link station for point ${point.x},${point.y} is ${stationWithPower.x}, ${stationWithPower.y} with power ${stationWithPower.power}`;
    } else
      return `No link station within reach for point ${point.x}, ${point.y}`;
  }

  render() {
    return (
      <div style={{ padding: "2%" }}>
        <p>Print output for points</p>
        <ul style={{ listStyle: "none", padding: 0, margin: 0 }}>
          {this.state.points.map((p, i) => {
            return (
              <li key={"point" + i}>{`${this.getStringPresentation(
                getBestLinkStation(this.state.stations, p),
                p
              )}`}</li>
            );
          })}
        </ul>
      </div>
    );
  }
}

export default App;
