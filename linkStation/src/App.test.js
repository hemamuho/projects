import React from "react";
import ReactDOM from "react-dom";
import expect from "expect";
import App from "./App";
import {
  getDistanceBetweenPoints,
  getPowerToPoint,
  getBestLinkStation,
  filterOutOfReach
} from "./logic";

it("calculates distance between two points correctly", () => {
  expect(getDistanceBetweenPoints({ x: 0, y: 0 }, { x: 0, y: 0 })).toEqual(0);
  expect(getDistanceBetweenPoints({ x: 0, y: 0 }, { x: 1, y: 0 })).toEqual(1);
  expect(getDistanceBetweenPoints({ x: 0, y: 0 }, { x: 0, y: 1 })).toEqual(1);
  expect(getDistanceBetweenPoints({ x: 0, y: 0 }, { x: 1, y: 1 })).toEqual(
    1.4142135623730951
  );
  expect(getDistanceBetweenPoints({ x: 0, y: 0 }, { x: 100, y: 100 })).toEqual(
    141.4213562373095
  );

  expect(getDistanceBetweenPoints({ x: 0, y: 0 }, { x: -1, y: -1 })).toEqual(
    1.4142135623730951
  );
  expect(getDistanceBetweenPoints({ x: -1, y: -1 }, { x: 0, y: 0 })).toEqual(
    1.4142135623730951
  );
  expect(getDistanceBetweenPoints({ x: 0, y: 0 }, { x: -1, y: 1 })).toEqual(
    1.4142135623730951
  );

  expect(getDistanceBetweenPoints({ x: -1, y: -1 }, { x: -2, y: -2 })).toEqual(
    1.4142135623730951
  );
  expect(getDistanceBetweenPoints({ x: -1, y: -1 }, { x: -1, y: -2 })).toEqual(
    1
  );
});

it("calculates power to point correctly", () => {
  const station = reach => {
    return { x: 0, y: 0, r: reach };
  };
  expect(getPowerToPoint(station(2), { x: 0, y: 1 }).power).toEqual(1);
  expect(getPowerToPoint(station(2), { x: 0, y: -1 }).power).toEqual(1);
  expect(getPowerToPoint(station(3), { x: 0, y: 1 }).power).toEqual(4);
  expect(getPowerToPoint(station(3), { x: 0, y: -1 }).power).toEqual(4);
});

it("filters out of reach stations", () => {
  const stations = [
    { x: 0, y: 0, r: 5 },
    { x: 0, y: 0, r: 10 },
    { x: 0, y: 0, r: 15 }
  ];
  let point = { x: 40, y: 40 };
  expect(filterOutOfReach(stations, point).length).toEqual(0);

  point = { x: 10, y: 10 };
  expect(filterOutOfReach(stations, point).length).toEqual(1);

  point = { x: 5, y: 5 };
  expect(filterOutOfReach(stations, point).length).toEqual(2);

  point = { x: 1, y: 1 };
  expect(filterOutOfReach(stations, point).length).toEqual(3);
});

it("gets the best station for a point", () => {
  const stations = [
    { x: 0, y: 0, r: 5 },
    { x: 10, y: 10, r: 1 },
    { x: 30, y: 30, r: 5 }
  ];

  const best = getBestLinkStation(stations, { x: 0, y: 0 });
  expect(best.x).toEqual(0);
  expect(best.y).toEqual(0);

  const best1 = getBestLinkStation(stations, { x: 10, y: 10 });
  expect(best1.x).toEqual(10);
  expect(best1.y).toEqual(10);

  const best2 = getBestLinkStation(stations, { x: 100, y: 100 });
  expect(best2).toEqual(null);
});
