const express = require("express");
const graphqlHTTP = require("express-graphql");
const { buildSchema } = require("graphql");
const { RandomDie } = require("./RandomDie.js");

const schema = buildSchema(
  `
  type RandomDie {
    numSides: Int!
    rollOnce: Int!
    roll(numRolls: Int!): [Int]
  }

  type Query {
    getDie(numSides: Int): RandomDie
    getMessages: [String]
  }
  type Mutation {
    setMessage(msg: String): String
  }

`
);

let messages = [];
// root provides an api endpoint for each schema
const root = {
  setMessage: ({ msg }) => {
    messages.push(msg);
    return msg;
    /*
    mutation{
      setMessage(msg:"test3")
    }
    */
  },
  getMessages: () => {
    return messages;
    /*{getMessages}*/
  },
  getDie: ({numSides}) => new RandomDie(numSides || 6)
  /*
  {
  getDie(numSides: 6) {
      rollOnce
      roll(numRolls: 3)
    }
  }
  */
};

let app = express();
app.use(
  "/graphql",
  graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true
  })
);

app.listen(4000);
console.log("running at http://localhost:4000/graphiql");
