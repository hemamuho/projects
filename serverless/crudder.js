'use strict'

const doc = require('dynamodb-doc'); // eslint-disable-line
const dynamo = new doc.DynamoDB();

module.exports = (tablename, event, context, callback) => {

    const done = (err, res) => {
        console.log('res', res);
        console.log('err', err);
        callback(null, {
            statusCode: err ? (err.statusCode || 500) : '200',
            body: err
                ? JSON.stringify(err)
                : JSON.stringify(res),
            headers: {
                'Content-Type': 'application/json',
            },
        });
    }

    console.log('EVENT', event);
    const item = JSON.parse(event.body);
    switch (event.httpMethod) {
        case 'DELETE':
            dynamo.deleteItem({
                TableName: tablename,
                Key: {name: item.name}
            }, done);
            break;
        case 'GET':
            dynamo.scan({
                TableName: tablename
            }, done);
            break;
        case 'POST':
            console.log('POSTED', item);
            dynamo.putItem({
                TableName: tablename,
                Item: item,
            }, done);
            break;
        case 'PUT':
            dynamo.updateItem({
                TableName: tablename,
                Item: item,
            }, done);
            break;
        default:
            done(new Error(`Unsupported method "${event.httpMethod}"`));
    }
};
