'use strict';

const crudder = require('./crudder');

module.exports.notesHandler = (event, context, callback) => {
    crudder('notes', event, context, callback);
}
