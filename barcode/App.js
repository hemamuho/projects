import React from 'react';
import { StyleSheet, Text, View, Image, Button } from 'react-native';
import Camera from 'react-native-camera';

export default class App extends React.Component {

  constructor(){
    super();
    this.state = {
      barcodes: [],
      inReadMode: false
    }
  }


  takePicture() {
    const options = {
    };
    this.camera.capture({metadata: options})
      .then((data) => console.log('WAT IMAGE', data))
      .catch(err => console.error(err));
  }

  render() {
    console.log('RENDER', this.state)
      return (
            <View style={styles.container}>
              <Image style={{width: 120, height: 50, marginBottom: 10}} source={require('./sc5.png')} />
              <View style={{width: 250, height: 250, marginBottom: 20}}>
              <Camera
                ref={(cam) => {
                  this.camera = cam;
                }}
                onBarCodeRead = { (data) =>{ 
                  if(this.state.inReadMode){
                    const barcodes = this.state.barcodes
                    barcodes.push(data)
                    this.setState({barcodes, inReadMode: false})
                  }
                }}
                style={styles.preview}
                aspect={Camera.constants.Aspect.fill}>
              </Camera>
              </View>
              {
                this.state.inReadMode
                ?
                <View style={{height: 100}}><Text>Please read a code</Text></View>
                :
                <Button style={{width: 100, height: 100, backgroundColor: '#ff2834'}} title="  +  " onPress={() => {this.setState({inReadMode: true})}}/>
              }
              <View style={styles.container}><Text>Scanned items:</Text>
              {
                this.state.barcodes.map((barcode,i) => <Text key={i}>Type: {barcode.type} - Value: {barcode.data}</Text>)
              }
              </View>
            </View>
      )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    marginTop: 20,
    alignItems: 'center'
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    color: '#000',
    padding: 10,
    margin: 40
  }
});
