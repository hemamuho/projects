import { combineReducers } from 'redux';

import { ADD_NOTE, UPDATE_NOTES, RECEIVE_NOTES } from './actions';

function notes(state = {
    notes: [],
    isFetching: false,
}, action) {
    switch (action.type) {
        case ADD_NOTE: {
            return Object.assign({}, state, {
                notes: [...state.notes, { name: '', content: '' }]
            });
        }
        case UPDATE_NOTES: {
            return Object.assign({}, state, {
                isFetching: true
            });
        }
        case RECEIVE_NOTES: {
            return Object.assign({}, state, {
                isFetching: false,
                notes: action.notes
            });
        }
        default:
            return state;
    }
}


const app = combineReducers({
    notes
});

export default app;
