import * as apicalls from '../utils/apicalls';

export const ADD_NOTE = 'ADD_NOTE';
export function addNote() {
    return {
        type: ADD_NOTE
    };
}


export const UPDATE_NOTES = 'UPDATE_NOTES';
export function updateNotes() {
    return {
        type: UPDATE_NOTES
    };
}

export const RECEIVE_NOTES = 'RECEIVE_NOTES';
export function receiveNotes(notes) {
    return {
        type: RECEIVE_NOTES,
        notes
    };
}

// TODO: error handling
export function fetchNotes() {
    return async (dispatch) => {
        dispatch(updateNotes);
        const notes = await apicalls.getNotes();
        dispatch(receiveNotes(notes));
    };
}

export function updateNote(note) {
    return async (dispatch) => {
        dispatch(updateNotes()); // bad naming
        await apicalls.updateNote(note);
        const notes = await apicalls.getNotes();
        dispatch(receiveNotes(notes));
    };
}

export function deleteNote(note) {
    return async (dispatch) => {
        dispatch(updateNotes());
        await apicalls.deleteNote(note);
        const notes = await apicalls.getNotes();
        dispatch(receiveNotes(notes));
    };
}
