import axios from 'axios';

const API_URL = 'https://4aqqhknqu4.execute-api.eu-west-1.amazonaws.com/dev/notes';

export async function getNotes() {
    try {
        const response = await axios.get(API_URL);
        return response.data.Items;
    } catch (err) {
        console.error('Getting notes failed', err);
        return new Error('Fetching notes failed.');
    }
}

export async function updateNote(note) {
    try {
        const response = await axios({
            url: API_URL,
            method: 'POST',
            data: JSON.stringify(note)
        });
        return response;
    } catch (err) {
        console.error('POST notes failed', err);
        return new Error(`Updating note ${note.name} failed.`);
    }
}

export async function deleteNote(note) {
    try {
        const response = await axios({
            url: API_URL,
            method: 'DELETE',
            data: JSON.stringify(note)
        });
        return response;
    } catch (err) {
        console.error(`Deleting note ${note.name} failed.`, err);
        return new Error(`Deleting note ${note.name} failed.`);
    }
}