import { PropTypes as pt } from 'react';

export const NoteDefinition = pt.shape({
    name: pt.string.isRequired,
    content: pt.string
});
