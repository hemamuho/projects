import React, { PropTypes as pt } from 'react';
import { TouchableOpacity, View, Text, StyleSheet } from 'react-native';

import { colors, fontSizes } from './constants';

const StyledButton = (props) => {
    const styles = StyleSheet.create({
        btnContainer: {
            borderWidth: 1,
            borderColor: props.type === 'newhover' ? colors.highlight : colors.mainDark,
            backgroundColor: props.type === 'newhover' ? colors.highlight : colors.white,
            elevation: 3,
            padding: 5,
            marginTop: 5,
            marginBottom: 5,
            borderRadius: 2,
        },
        text: {
            fontSize: fontSizes.button
        },
        save: {// eslint-disable-line react-native/no-unused-styles
            color: colors.green
        },
        delete: {// eslint-disable-line react-native/no-unused-styles
            color: colors.red
        },
        newhover: { // eslint-disable-line react-native/no-unused-styles
            color: colors.white
        }
    });


    const title = getTitle(props.type);
    return (
        <TouchableOpacity onPress={props.onPress}>
            <View
                style={styles.btnContainer}
                removeClippedSubviews
            >
                <Text
                    style={[styles.text, styles[props.type]]}
                    title={title}
                >
                    {title}
                </Text>
            </View>
        </TouchableOpacity>
    );
};

function getTitle(type) {
    switch (type) {
        case 'delete':
            return 'Delete';
        case 'save':
            return 'Save';
        case 'newhover':
            return 'New';
        default:
            return 'No title';
    }
}

StyledButton.propTypes = {
    type: pt.oneOf(['save', 'delete', 'newhover']).isRequired,
    onPress: pt.func,
};

export default StyledButton;