export const colors = {
    mainDark: '#333344',
    mainLight: '#eeeeee',
    highlight: '#bb4455',
    itemBackground: '#443344',
    green: '#006600',
    red: '#cc3300',
    white: '#fff'
};

export const fontSizes = {
    h1: 20,
    p: 16,
    button: 18
};