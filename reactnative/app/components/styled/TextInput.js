import React, { PropTypes as pt } from 'react';
import { StyleSheet, TextInput } from 'react-native';
import { colors, fontSizes } from './constants';

const StyledTextInput = (props) => {
    const height = fontSizes.h1 * (props.multiline || 1) + 5;
    const styles = StyleSheet.create({
        input: {
            height,
            fontSize: fontSizes.p,
            color: colors.mainDark,
            backgroundColor: colors.white,
            marginBottom: 5,
            padding: 0, // stupid android
            paddingLeft: 5,
            paddingRight: 5
        }
    });

    return (
        <TextInput
            style={styles.input}
            value={props.value}
            onChangeText={props.onChangeText}
            multiline={Boolean(props.multiline)}
            numberOfLines={props.multiline || 1}
            inlineImagePadding={0}
            underlineColorAndroid="transparent"
        />
    );
};

StyledTextInput.propTypes = {
    value: pt.string.isRequired,
    onChangeText: pt.func,
    multiline: pt.number
};

export default StyledTextInput;