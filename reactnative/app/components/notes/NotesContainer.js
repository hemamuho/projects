import React, { PropTypes as pt } from 'react';
import { ListView, View, Button, StyleSheet, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';

import { NoteDefinition } from '../../utils/propTypes';
import { addNote, fetchNotes, updateNote, deleteNote } from '../../redux/actions';
import Note from './Note';
import StyledButton from '../styled/Button';

const Container = ({ notes, isFetching, dispatch }) => {
    if (notes.length === 0) dispatch(fetchNotes());

    if (isFetching) return <View style={styles.container}><ActivityIndicator animating size={'large'} /></View>;

    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    const noteSource = ds.cloneWithRows(notes);

    return (
        <View style={styles.container}>
            <ListView
                style={{ alignSelf: 'stretch' }}
                dataSource={noteSource}
                renderRow={(n) => {
                    return (
                        <Note
                            key={n.name}
                            note={n}
                            updateNote={(note) => {
                                dispatch(updateNote(note));
                            }}
                            deleteNote={((note) => {
                                dispatch(deleteNote(note));
                            })}
                        />
                    );
                }}
            />
            <View
                style={{ position: 'absolute', bottom: 20, right: 20 }}
                elevation={10}
            >
                <StyledButton
                    type="newhover"
                    onPress={() => {
                        dispatch(addNote());
                    }}
                />
            </View>
        </View>
    );
};

Container.propTypes = {
    notes: pt.arrayOf(NoteDefinition),
    dispatch: React.PropTypes.func,
    isFetching: React.PropTypes.bool
};

const styles = StyleSheet.create({
    container: {
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center',
        flexGrow: 1
    }
});

const NotesContainer = connect(
    (state) => {
        return {
            notes: state.notes.notes,
            isFetching: state.notes.isFetching
        };
    },
)(Container);
export default NotesContainer;
