import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';

import { NoteDefinition } from '../../utils/propTypes';
import StyledButton from '../styled/Button';
import StyledTextInput from '../styled/TextInput';

import { colors } from '../styled/constants';

class Note extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: this.props.note.name,
            content: this.props.note.content
        };
    }

    render() {
        return (
            <View
                style={styles.noteContainer}
                elevation={5}
            >
                <StyledTextInput
                    value={this.state.name}
                    onChangeText={(text) => { this.setState({ name: text }); }}
                />
                <StyledTextInput
                    value={this.state.content}
                    onChangeText={(text) => { this.setState({ content: text }); }}
                    multiline={3}
                />
                <View style={styles.buttonContainer}>
                    <StyledButton
                        type="delete"
                        onPress={() => {
                            this.props.deleteNote({ name: this.state.name, content: this.state.content });
                        }}
                    />
                    <StyledButton
                        type="save"
                        onPress={() => {
                            this.props.updateNote({ name: this.state.name, content: this.state.content });
                        }}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    noteContainer: {
        backgroundColor: colors.itemBackground,
        padding: 10,
        margin: 5,
        alignSelf: 'stretch'
    },
    buttonContainer: {
        flexDirection: 'row',
        alignItems: 'stretch',
        justifyContent: 'space-between'
    }
});

Note.propTypes = {
    note: NoteDefinition,
    updateNote: React.PropTypes.func,
    deleteNote: React.PropTypes.func,

};

export default Note;
