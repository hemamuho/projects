import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from '../redux/store';

import NotesContainer from './notes/NotesContainer';

export default class App extends Component {
    constructor() {
        super();
        this.state = {
            notes: []
        };
    }

    render() {
        return (
            <Provider store={store}>
                <NotesContainer />
            </Provider>
        );
    }
}
