# PLAYGROUND #

Random stuff written while learning new technolgies.

### Contents ###

* animate: quick demo with react-interactable
* bacon.js: simple form builder with bacon.js
* barcode: demo a simple react-native app that reads barcodes using a third-party plugin
* graphql: trying out graphql
* linkstation: react demo finding the strongest link for a station
* react-vr: just a created app, no own content
* reactnative: old app for editing notes with serverless backend
* serverless: backend for notes
* sc5-blog-reader: simple blog reader getting data from a wordpress site with different technologies
    * react
        * redux + sagas
        * mobx
        * freactal
    * next.js
    * react-native

### Who do I talk to? ###

* Mattias Muhonen
* hmmuhonen@gmail.com