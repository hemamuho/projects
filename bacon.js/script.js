function onInputById(inputId) {
  return $(`#${inputId}`)
    .asEventStream("keyup")
    .map(e => e.target.value)
    .debounce(150)
    .assign(val => $(`#${inputId}-assign`).text(val));
}
// some sort of formbuilder component that renders itself
class FormBuilder {
  constructor(fields) {
    this.fields = fields;
  }

  template() {
    let template = "";
    this.fields.forEach(
      f =>
        template += `
    <label for="${f}">${f}</label>
    <input id="${f}"/>
    <p>Assign: <span id="${f + "-assign"}"/></p>
    `
    );
    template += `<br/><button style="margin: 10px; padding: 10px;" id="button">Ok</button>`;
    return template;
  }

  functionality() {
    // set up button
    $("#button").attr("disabled", true);
    $("#button").on("click", () => {
      this.fields.forEach(f => console.log(f, "=", $(`#${f}`).val()));
    });

    // simple form check
    Bacon.combineWith(
      (...args) => args.includes(""),
      this.fields.map(f => onInputById(f))
    ).onValue(valid => {
      $("#button").attr("disabled", valid);
    });
  }

  renderTo(htmlId) {
    $(`#${htmlId}`).html(this.template());
    this.functionality();
  }
}

$(document).ready(function() {
  console.log("Document ready, script running.");
  const Form = new FormBuilder(["firstname", "lastname"]);
  Form.renderTo("form");
});
