"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = require("babel-runtime/regenerator");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require("babel-runtime/helpers/asyncToGenerator");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = require("babel-runtime/core-js/object/get-prototype-of");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require("babel-runtime/helpers/classCallCheck");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require("babel-runtime/helpers/createClass");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require("babel-runtime/helpers/possibleConstructorReturn");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require("babel-runtime/helpers/inherits");

var _inherits3 = _interopRequireDefault(_inherits2);

var _taggedTemplateLiteral2 = require("babel-runtime/helpers/taggedTemplateLiteral");

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _isomorphicUnfetch = require("isomorphic-unfetch");

var _isomorphicUnfetch2 = _interopRequireDefault(_isomorphicUnfetch);

var _styledComponents = require("styled-components");

var _styledComponents2 = _interopRequireDefault(_styledComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = "/Users/mattias/work/playground/sc5-blog-reader/next.js/pages/index.js?entry";

var _templateObject = (0, _taggedTemplateLiteral3.default)(["\n  from{\n    transform: rotate(0deg)\n  }\n  to{\n    transform: rotate(360deg)\n  }\n"], ["\n  from{\n    transform: rotate(0deg)\n  }\n  to{\n    transform: rotate(360deg)\n  }\n"]),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(["\n  display: flex;\n  max-height: 60px;\n  borderBottom: 1px solid #333;\n  align-items: flex-end;\n  img{\n    width: 100px;\n    animation: ", " 2s linear infinite\n  }\n  h2{\n    margin: 0;\n    padding: 0;\n    margin-left: 20px;\n  }\n  a{\n    text-decoration: none;\n    margin: 0 10px 0 10px;\n  }\n  .active{\n    font-weight: bold;\n  }\n"], ["\n  display: flex;\n  max-height: 60px;\n  borderBottom: 1px solid #333;\n  align-items: flex-end;\n  img{\n    width: 100px;\n    animation: ", " 2s linear infinite\n  }\n  h2{\n    margin: 0;\n    padding: 0;\n    margin-left: 20px;\n  }\n  a{\n    text-decoration: none;\n    margin: 0 10px 0 10px;\n  }\n  .active{\n    font-weight: bold;\n  }\n"]);

var rotate = (0, _styledComponents.keyframes)(_templateObject);
var Header = _styledComponents2.default.header(_templateObject2, rotate);

var _class = function (_React$Component) {
  (0, _inherits3.default)(_class, _React$Component);

  function _class(props) {
    (0, _classCallCheck3.default)(this, _class);

    var _this = (0, _possibleConstructorReturn3.default)(this, (_class.__proto__ || (0, _getPrototypeOf2.default)(_class)).call(this, props));

    _this.state = {
      posts: [],
      selectedPost: ""
    };
    _this.getPostContent = _this.getPostContent.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(_class, [{
    key: "componentWillMount",
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee() {
        var posts, response;
        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                posts = [];
                _context.next = 3;
                return (0, _isomorphicUnfetch2.default)("https://sc5.io/wp-json/wp/v2/posts");

              case 3:
                response = _context.sent;

                if (!(response.status === 200)) {
                  _context.next = 8;
                  break;
                }

                _context.next = 7;
                return response.json();

              case 7:
                posts = _context.sent;

              case 8:
                this.setState({
                  posts: posts
                });

              case 9:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function componentWillMount() {
        return _ref.apply(this, arguments);
      }

      return componentWillMount;
    }()
  }, {
    key: "getPostContent",
    value: function getPostContent(post) {
      return {
        __html: post.content.rendered
      };
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 65
        }
      }, _react2.default.createElement(Header, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 66
        }
      }, _react2.default.createElement("img", { src: "/static/sc5-logo.svg", alt: "SC5", __source: {
          fileName: _jsxFileName,
          lineNumber: 67
        }
      })), _react2.default.createElement("ul", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 69
        }
      }, this.state.posts.map(function (post) {
        return _react2.default.createElement("li", {
          key: post.id,
          onClick: function onClick() {
            _this2.setState({ selectedPost: post });
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 71
          }
        }, post.title.rendered);
      })), this.state.selectedPost && _react2.default.createElement("div", {
        dangerouslySetInnerHTML: this.getPostContent(this.state.selectedPost),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 82
        }
      }));
    }
  }]);

  return _class;
}(_react2.default.Component);

/*
    import fetch from 'isomorphic-unfetch'
    
    
    const Index = (props) => {
        return <div>
        <h1>Hi {props.title}</h1>
        <ul>
        {props.posts.map(post => <li onClick={() => console.log(post)}>{post.title.rendered}</li>)}
        </ul>
        </div>
    }
    
    Index.getInitialProps = async function(){
        let posts = [];
        const response = await fetch(`https://sc5.io/wp-json/wp/v2/posts`);
        if( response.status === 200) {
            posts = await response.json()
        }
        return {
            title: 'test',
            posts
        }
    }
    
    export default Index;

    */

exports.default = _class;