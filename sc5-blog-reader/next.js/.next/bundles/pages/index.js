
          window.__NEXT_REGISTER_PAGE('/', function() {
            var comp = module.exports =
webpackJsonp([5],{

/***/ 559:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(__resourceQuery) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(64);

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(63);

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(44);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(18);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(19);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(46);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(45);

var _inherits3 = _interopRequireDefault(_inherits2);

var _taggedTemplateLiteral2 = __webpack_require__(562);

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _react = __webpack_require__(11);

var _react2 = _interopRequireDefault(_react);

var _isomorphicUnfetch = __webpack_require__(567);

var _isomorphicUnfetch2 = _interopRequireDefault(_isomorphicUnfetch);

var _styledComponents = __webpack_require__(568);

var _styledComponents2 = _interopRequireDefault(_styledComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = "/Users/mattias/work/playground/sc5-blog-reader/next.js/pages/index.js?entry";

var _templateObject = (0, _taggedTemplateLiteral3.default)(["\n  from{\n    transform: rotate(0deg)\n  }\n  to{\n    transform: rotate(360deg)\n  }\n"], ["\n  from{\n    transform: rotate(0deg)\n  }\n  to{\n    transform: rotate(360deg)\n  }\n"]),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(["\n  display: flex;\n  max-height: 60px;\n  borderBottom: 1px solid #333;\n  align-items: flex-end;\n  img{\n    width: 100px;\n    animation: ", " 2s linear infinite\n  }\n  h2{\n    margin: 0;\n    padding: 0;\n    margin-left: 20px;\n  }\n  a{\n    text-decoration: none;\n    margin: 0 10px 0 10px;\n  }\n  .active{\n    font-weight: bold;\n  }\n"], ["\n  display: flex;\n  max-height: 60px;\n  borderBottom: 1px solid #333;\n  align-items: flex-end;\n  img{\n    width: 100px;\n    animation: ", " 2s linear infinite\n  }\n  h2{\n    margin: 0;\n    padding: 0;\n    margin-left: 20px;\n  }\n  a{\n    text-decoration: none;\n    margin: 0 10px 0 10px;\n  }\n  .active{\n    font-weight: bold;\n  }\n"]);

var rotate = (0, _styledComponents.keyframes)(_templateObject);
var Header = _styledComponents2.default.header(_templateObject2, rotate);

var _class = function (_React$Component) {
  (0, _inherits3.default)(_class, _React$Component);

  function _class(props) {
    (0, _classCallCheck3.default)(this, _class);

    var _this = (0, _possibleConstructorReturn3.default)(this, (_class.__proto__ || (0, _getPrototypeOf2.default)(_class)).call(this, props));

    _this.state = {
      posts: [],
      selectedPost: ""
    };
    _this.getPostContent = _this.getPostContent.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(_class, [{
    key: "componentWillMount",
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee() {
        var posts, response;
        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                posts = [];
                _context.next = 3;
                return (0, _isomorphicUnfetch2.default)("https://sc5.io/wp-json/wp/v2/posts");

              case 3:
                response = _context.sent;

                if (!(response.status === 200)) {
                  _context.next = 8;
                  break;
                }

                _context.next = 7;
                return response.json();

              case 7:
                posts = _context.sent;

              case 8:
                this.setState({
                  posts: posts
                });

              case 9:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function componentWillMount() {
        return _ref.apply(this, arguments);
      }

      return componentWillMount;
    }()
  }, {
    key: "getPostContent",
    value: function getPostContent(post) {
      return {
        __html: post.content.rendered
      };
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 65
        }
      }, _react2.default.createElement(Header, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 66
        }
      }, _react2.default.createElement("img", { src: "/static/sc5-logo.svg", alt: "SC5", __source: {
          fileName: _jsxFileName,
          lineNumber: 67
        }
      })), _react2.default.createElement("ul", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 69
        }
      }, this.state.posts.map(function (post) {
        return _react2.default.createElement("li", {
          key: post.id,
          onClick: function onClick() {
            _this2.setState({ selectedPost: post });
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 71
          }
        }, post.title.rendered);
      })), this.state.selectedPost && _react2.default.createElement("div", {
        dangerouslySetInnerHTML: this.getPostContent(this.state.selectedPost),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 82
        }
      }));
    }
  }]);

  return _class;
}(_react2.default.Component);

/*
    import fetch from 'isomorphic-unfetch'
    
    
    const Index = (props) => {
        return <div>
        <h1>Hi {props.title}</h1>
        <ul>
        {props.posts.map(post => <li onClick={() => console.log(post)}>{post.title.rendered}</li>)}
        </ul>
        </div>
    }
    
    Index.getInitialProps = async function(){
        let posts = [];
        const response = await fetch(`https://sc5.io/wp-json/wp/v2/posts`);
        if( response.status === 200) {
            posts = await response.json()
        }
        return {
            title: 'test',
            posts
        }
    }
    
    export default Index;

    */

exports.default = _class;

 ;(function register() { /* react-hot-loader/webpack */ if (true) { if (typeof __REACT_HOT_LOADER__ === 'undefined') { return; } if (typeof module.exports === 'function') { __REACT_HOT_LOADER__.register(module.exports, 'module.exports', "/Users/mattias/work/playground/sc5-blog-reader/next.js/pages/index.js"); return; } for (var key in module.exports) { if (!Object.prototype.hasOwnProperty.call(module.exports, key)) { continue; } var namedExport = void 0; try { namedExport = module.exports[key]; } catch (err) { continue; } __REACT_HOT_LOADER__.register(namedExport, key, "/Users/mattias/work/playground/sc5-blog-reader/next.js/pages/index.js"); } } })();
    (function (Component, route) {
      if (false) return
      if (false) return

      var qs = __webpack_require__(83)
      var params = qs.parse(__resourceQuery.slice(1))
      if (params.entry == null) return

      module.hot.accept()
      Component.__route = route

      if (module.hot.status() === 'idle') return

      var components = next.router.components
      for (var r in components) {
        if (!components.hasOwnProperty(r)) continue

        if (components[r].Component.__route === route) {
          next.router.update(r, Component)
        }
      }
    })(module.exports.default || module.exports, "/")
  
/* WEBPACK VAR INJECTION */}.call(exports, "?entry"))

/***/ }),

/***/ 569:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(559);


/***/ })

},[569]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnVuZGxlcy9wYWdlcy9pbmRleC5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3BhZ2VzP2JhNDAyZWIiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0IGZldGNoIGZyb20gXCJpc29tb3JwaGljLXVuZmV0Y2hcIjtcbmltcG9ydCBzdHlsZWQsIHsga2V5ZnJhbWVzIH0gZnJvbSBcInN0eWxlZC1jb21wb25lbnRzXCI7XG5cbmNvbnN0IHJvdGF0ZSA9IGtleWZyYW1lc2BcbiAgZnJvbXtcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKVxuICB9XG4gIHRve1xuICAgIHRyYW5zZm9ybTogcm90YXRlKDM2MGRlZylcbiAgfVxuYDtcbmNvbnN0IEhlYWRlciA9IHN0eWxlZC5oZWFkZXJgXG4gIGRpc3BsYXk6IGZsZXg7XG4gIG1heC1oZWlnaHQ6IDYwcHg7XG4gIGJvcmRlckJvdHRvbTogMXB4IHNvbGlkICMzMzM7XG4gIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcbiAgaW1ne1xuICAgIHdpZHRoOiAxMDBweDtcbiAgICBhbmltYXRpb246ICR7cm90YXRlfSAycyBsaW5lYXIgaW5maW5pdGVcbiAgfVxuICBoMntcbiAgICBtYXJnaW46IDA7XG4gICAgcGFkZGluZzogMDtcbiAgICBtYXJnaW4tbGVmdDogMjBweDtcbiAgfVxuICBhe1xuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICBtYXJnaW46IDAgMTBweCAwIDEwcHg7XG4gIH1cbiAgLmFjdGl2ZXtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgfVxuYDtcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgcG9zdHM6IFtdLFxuICAgICAgc2VsZWN0ZWRQb3N0OiBcIlwiXG4gICAgfTtcbiAgICB0aGlzLmdldFBvc3RDb250ZW50ID0gdGhpcy5nZXRQb3N0Q29udGVudC5iaW5kKHRoaXMpO1xuICB9XG5cbiAgYXN5bmMgY29tcG9uZW50V2lsbE1vdW50KCkge1xuICAgIGxldCBwb3N0cyA9IFtdO1xuICAgIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgZmV0Y2goYGh0dHBzOi8vc2M1LmlvL3dwLWpzb24vd3AvdjIvcG9zdHNgKTtcbiAgICBpZiAocmVzcG9uc2Uuc3RhdHVzID09PSAyMDApIHtcbiAgICAgIHBvc3RzID0gYXdhaXQgcmVzcG9uc2UuanNvbigpO1xuICAgIH1cbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIHBvc3RzXG4gICAgfSk7XG4gIH1cblxuICBnZXRQb3N0Q29udGVudChwb3N0KSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIF9faHRtbDogcG9zdC5jb250ZW50LnJlbmRlcmVkXG4gICAgfTtcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdj5cbiAgICAgICAgPEhlYWRlcj5cbiAgICAgICAgICA8aW1nIHNyYz1cIi9zdGF0aWMvc2M1LWxvZ28uc3ZnXCIgYWx0PVwiU0M1XCIgLz5cbiAgICAgICAgPC9IZWFkZXI+XG4gICAgICAgIDx1bD5cbiAgICAgICAgICB7dGhpcy5zdGF0ZS5wb3N0cy5tYXAocG9zdCA9PiAoXG4gICAgICAgICAgICA8bGlcbiAgICAgICAgICAgICAga2V5PXtwb3N0LmlkfVxuICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHNlbGVjdGVkUG9zdDogcG9zdCB9KTtcbiAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAge3Bvc3QudGl0bGUucmVuZGVyZWR9XG4gICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICkpfVxuICAgICAgICA8L3VsPlxuICAgICAgICB7dGhpcy5zdGF0ZS5zZWxlY3RlZFBvc3QgJiZcbiAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICBkYW5nZXJvdXNseVNldElubmVySFRNTD17dGhpcy5nZXRQb3N0Q29udGVudChcbiAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5zZWxlY3RlZFBvc3RcbiAgICAgICAgICAgICl9XG4gICAgICAgICAgLz59XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cbi8qXG4gICAgaW1wb3J0IGZldGNoIGZyb20gJ2lzb21vcnBoaWMtdW5mZXRjaCdcbiAgICBcbiAgICBcbiAgICBjb25zdCBJbmRleCA9IChwcm9wcykgPT4ge1xuICAgICAgICByZXR1cm4gPGRpdj5cbiAgICAgICAgPGgxPkhpIHtwcm9wcy50aXRsZX08L2gxPlxuICAgICAgICA8dWw+XG4gICAgICAgIHtwcm9wcy5wb3N0cy5tYXAocG9zdCA9PiA8bGkgb25DbGljaz17KCkgPT4gY29uc29sZS5sb2cocG9zdCl9Pntwb3N0LnRpdGxlLnJlbmRlcmVkfTwvbGk+KX1cbiAgICAgICAgPC91bD5cbiAgICAgICAgPC9kaXY+XG4gICAgfVxuICAgIFxuICAgIEluZGV4LmdldEluaXRpYWxQcm9wcyA9IGFzeW5jIGZ1bmN0aW9uKCl7XG4gICAgICAgIGxldCBwb3N0cyA9IFtdO1xuICAgICAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IGZldGNoKGBodHRwczovL3NjNS5pby93cC1qc29uL3dwL3YyL3Bvc3RzYCk7XG4gICAgICAgIGlmKCByZXNwb25zZS5zdGF0dXMgPT09IDIwMCkge1xuICAgICAgICAgICAgcG9zdHMgPSBhd2FpdCByZXNwb25zZS5qc29uKClcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgdGl0bGU6ICd0ZXN0JyxcbiAgICAgICAgICAgIHBvc3RzXG4gICAgICAgIH1cbiAgICB9XG4gICAgXG4gICAgZXhwb3J0IGRlZmF1bHQgSW5kZXg7XG5cbiAgICAqL1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vcGFnZXM/ZW50cnkiXSwibWFwcGluZ3MiOiI7QTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBOzs7QUFBQTtBQUNBOzs7QUFBQTtBQUNBOzs7Ozs7Ozs7O0FBQ0E7QUFRQTtBQUNBOztBQXVCQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBRUE7QUFFQTtBQUhBO0FBR0E7QUFDQTs7Ozs7Ozs7Ozs7QUFHQTtBQUFBOztBQUNBO0FBQ0E7QUFEQTs7O0FBQ0E7Ozs7OztBQUNBO0FBQ0E7QUFEQTs7O0FBRUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFHQTs7QUFFQTtBQUFBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQUE7O0FBQUE7QUFDQTtBQURBO0FBQUE7O0FBQ0E7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUVBO0FBRkE7QUFFQTs7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7O0FBQUE7QUFNQTtBQU5BO0FBQ0E7QUFTQTtBQUVBOztBQURBO0FBT0E7QUFQQTtBQUNBOzs7OztBQS9DQTtBQUNBO0FBdURBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0EiLCJzb3VyY2VSb290IjoiIn0=
            return { page: comp.default }
          })
        