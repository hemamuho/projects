import React from "react";
import fetch from "isomorphic-unfetch";
import styled, { keyframes } from "styled-components";

const rotate = keyframes`
  from{
    transform: rotate(0deg)
  }
  to{
    transform: rotate(360deg)
  }
`;
const Header = styled.header`
  display: flex;
  max-height: 60px;
  borderBottom: 1px solid #333;
  align-items: flex-end;
  img{
    width: 100px;
    animation: ${rotate} 2s linear infinite
  }
  h2{
    margin: 0;
    padding: 0;
    margin-left: 20px;
  }
  a{
    text-decoration: none;
    margin: 0 10px 0 10px;
  }
  .active{
    font-weight: bold;
  }
`;

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      selectedPost: ""
    };
    this.getPostContent = this.getPostContent.bind(this);
  }

  async componentWillMount() {
    let posts = [];
    const response = await fetch(`https://sc5.io/wp-json/wp/v2/posts`);
    if (response.status === 200) {
      posts = await response.json();
    }
    this.setState({
      posts
    });
  }

  getPostContent(post) {
    return {
      __html: post.content.rendered
    };
  }

  render() {
    return (
      <div>
        <Header>
          <img src="/static/sc5-logo.svg" alt="SC5" />
        </Header>
        <ul>
          {this.state.posts.map(post => (
            <li
              key={post.id}
              onClick={() => {
                this.setState({ selectedPost: post });
              }}
            >
              {post.title.rendered}
            </li>
          ))}
        </ul>
        {this.state.selectedPost &&
          <div
            dangerouslySetInnerHTML={this.getPostContent(
              this.state.selectedPost
            )}
          />}
      </div>
    );
  }
}

/*
    import fetch from 'isomorphic-unfetch'
    
    
    const Index = (props) => {
        return <div>
        <h1>Hi {props.title}</h1>
        <ul>
        {props.posts.map(post => <li onClick={() => console.log(post)}>{post.title.rendered}</li>)}
        </ul>
        </div>
    }
    
    Index.getInitialProps = async function(){
        let posts = [];
        const response = await fetch(`https://sc5.io/wp-json/wp/v2/posts`);
        if( response.status === 200) {
            posts = await response.json()
        }
        return {
            title: 'test',
            posts
        }
    }
    
    export default Index;

    */
