import { actions } from "./actions";

const initialState = {
  posts: [],
  fetchingPosts: false,
  selectedPost: null
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case actions.FETCHING_POSTS:
      return Object.assign({}, state, { fetchingPosts: true });
    case actions.FETCH_POSTS_SUCCEED:
      return Object.assign({}, state, {
        posts: action.posts,
        fetchingPosts: false
      });
    case actions.FETCH_POSTS_FAILED:
      return Object.assign({}, {}, {});
    case actions.SELECT_POST:
      return Object.assign({}, state, { selectedPost: action.post });
    default:
      return state;
  }
}
