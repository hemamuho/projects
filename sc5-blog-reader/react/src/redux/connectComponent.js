/**
 * Redux container
 */
import { connect } from "react-redux";
import { fetchPosts, selectPost } from "./actions";

export default function connectComponent(Component) {
  return connect(
    state => ({
      posts: state.posts,
      fetchingPosts: state.fetchingPosts,
      selectedPost: state.selectedPost
    }),
    dispatch => ({
      fetchPosts: () => dispatch(fetchPosts()),
      selectPost: post => dispatch(selectPost(post))
    })
  )(Component);
}
