import { put, call } from "redux-saga/effects";
import { fetchPosts } from "./sagas";

import api from "../utils/api";
import { fetchPostsSuccess, fetchingPosts } from "./actions";

describe("fetchPosts saga", () => {
  const gen = fetchPosts();

  it("puts fetchingPosts", () => {
    const test = gen.next().value;
    expect(test).toEqual(put(fetchingPosts()));
  });

  it('calls api to fetch posts', () => {
    const test = gen.next().value
    expect(test).toEqual(call(api.fetchPosts))
  })

  it('puts fetchsuccess', () => {
    const mockPosts = []
    const test = gen.next(mockPosts).value
    expect(test).toEqual(put(fetchPostsSuccess(mockPosts)))
  })

  it('should be done', () => {
    const test = gen.next()
    expect(test).toEqual({done: true, value: undefined})
  })

});

/**
 * redux saga example but we are running with jest
 
test("fetchposts saga test", assert => {

  assert.deepEqual(
    gen.next().value,
    put(fetchingPosts()),
    "calls api to fetch posts"
  );

  assert.deepEqual(
    gen.next().value,
    call(api.fetchPosts),
    "calls api to fetch posts"
  );

  const mockPosts = [];
  assert.deepEqual(
    gen.next(mockPosts).value,
    put(fetchPostsSuccess(mockPosts)),
    "dispatches success posts1"
  );

  assert.deepEqual(
    gen.next(),
    { done: true, value: undefined },
    "increment must be done"
  );

  assert.end();
});
*/
