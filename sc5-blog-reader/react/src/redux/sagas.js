import { actions, fetchingPosts, fetchPostsSuccess } from "./actions";
import { call, takeEvery, put } from "redux-saga/effects";
import api from "../utils/api";

export function* fetchPosts() {
  yield put(fetchingPosts())
  const posts = yield call(api.fetchPosts);
  yield put(fetchPostsSuccess(posts));
}

export default function* rootSaga() {
  yield takeEvery(actions.FETCH_POSTS, fetchPosts);
}
