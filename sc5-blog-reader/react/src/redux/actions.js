export const actions = {
  FETCH_POSTS: "FETCH_POSTS",
  FETCHING_POSTS: "FETCHING_POSTS",
  FETCH_POSTS_SUCCEED: "FETCH_POSTS_SUCCEED",
  FETCH_POSTS_FAILED: "FETCH_POSTS_FAILED",
  SELECT_POST: "SELECT_POST"
};

export function fetchPosts() {
  return {
    type: actions.FETCH_POSTS
  };
}

export function fetchingPosts(){
  return {
    type: actions.FETCHING_POSTS
  }
}

export function fetchPostsSuccess(posts) {
  return {
    type: actions.FETCH_POSTS_SUCCEED,
    posts
  };
}

export function selectPost(post){
    return {
        type: actions.SELECT_POST,
        post
    }
}