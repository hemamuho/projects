import React from "react";
import { Provider } from "react-redux";
import { FormattedMessage } from "react-intl";

import store from "./store";
import BlogReader from "../components/BlogReader";

import connectComponent from './connectComponent'

const BlogReaderRedux = connectComponent(BlogReader);

/**
 * export a independent redux module
 */
const ReduxApp = () => (
  <Provider store={store}>
    <BlogReaderRedux
      title={
        <FormattedMessage
          id="redux"
          defaultMessage="Redux"
          description="Redux-title"
        />
      }
    />
  </Provider>
);
export default ReduxApp;
