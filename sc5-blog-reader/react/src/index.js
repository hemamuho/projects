import React from "react";
import ReactDOM from "react-dom";
import { IntlProvider } from "react-intl";
import logo from "./img/sc5-logo.svg";
import styled, { keyframes } from "styled-components";

import ReduxApp from "./redux/ReduxApp";
import MobxApp from "./mobx/MobxApp";
import FreactalApp from "./freactal/FreactalApp";

import {
  BrowserRouter as Router,
  Route,
  Link,
  NavLink
} from "react-router-dom";

const routes = {
  REDUX: "redux",
  MOBX: "mobx",
  FREACTAL: "freactal"
};

const rotate = keyframes`
  from{
    transform: rotate(0deg)
  }
  to{
    transform: rotate(360deg)
  }
`;

const App = styled.div`
  display: flex;
  align-self: stretch;
  flex-direction: column;
`;

const Header = styled.header`
  display: flex;
  max-height: 60px;
  borderBottom: 1px solid #333;
  align-items: flex-end;
  img{
    width: 100px;
    animation: ${rotate} 2s linear infinite
  }
  h2{
    margin: 0;
    padding: 0;
    margin-left: 20px;
  }
  a{
    text-decoration: none;
    margin: 0 10px 0 10px;
  }
  .active{
    font-weight: bold;
  }
`;
export const Render = <IntlProvider locale="en" messages={{}}>
    <Router>
      {/* 
      reac-intl doesn't scale very well
      - defineMessages in one place
      - pass formatMessage in context to all components
      also adds span around every text
      */}
      <App>
        <Header>
          <img src={logo} alt="SC5" />
          <h2>
            Blog reader
          </h2>
          <Link to="/">Home</Link>
          <NavLink activeClassName="active" to={`/${routes.REDUX}`}>
            With Redux
          </NavLink>
          <NavLink activeClassName="active" to={`/${routes.MOBX}`}>
            With mobx
          </NavLink>
          <NavLink activeClassName="active" to={`/${routes.FREACTAL}`}>
            With freactal
          </NavLink>
        </Header>
        <Route
          path=""
          component={({ match }) =>
            match.isExact && <h2>Select a framework</h2>}
        />
        <div>
          <Route path={`/${routes.REDUX}`} component={ReduxApp} />
          <Route path={`/${routes.MOBX}`} component={MobxApp} />
          <Route
            path={`/${routes.FREACTAL}`}
            component={() => <FreactalApp />}
          />
        </div>
      </App>
    </Router>
  </IntlProvider>

ReactDOM.render(
  Render,
  document.getElementById("root")
);
