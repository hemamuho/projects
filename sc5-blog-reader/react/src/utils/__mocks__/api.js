/**
 * For tests that rely on api (redux doesn't)
 */
console.log('MOCK API')
const api = {
  fetchPosts: () => {
    console.log("Mockapi fetchPosts");
    return new Promise((resolve, reject)=>{
        process.nextTick(() => {
            resolve([1,2])
        })
    })
  }
};
export default api;