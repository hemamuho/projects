import fetch from 'isomorphic-unfetch'; // works on client and server next.js
const apiUrl = "https://sc5.io/wp-json/wp/v2";

function genericErrorHandler(err) {
  console.error("Something went wrong while calling api", err);
}

const api = {
  fetchPosts: async () => {
    const response = await fetch(`${apiUrl}/posts`);
    if (response.status === 200) {
      const posts = await response.json();
      return posts;
    } else genericErrorHandler(response);
  }
};

export default api;
