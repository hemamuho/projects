import React from 'react';
import styled from 'styled-components';
import { FormattedMessage } from 'react-intl';

const SelectPost = styled.p`
  font-weight: bold;
  text-align: center;
  color: red;
`;

export default function Post(props) {
    return (
    <div>
      {props.post
        ? <p
            dangerouslySetInnerHTML={{
                __html: props.post.excerpt.rendered
            }}
          />
        : <SelectPost>
            <FormattedMessage
              id="select-a-post"
              defaultMessage="Select a post to read it"
              description="Select a post -text shown when post not selected"
            />
          </SelectPost>}
    </div>
  );
}
