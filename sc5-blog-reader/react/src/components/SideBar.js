import React from 'react';
import styled from 'styled-components';
import { FormattedMessage } from 'react-intl';

const FetchingPosts = styled.p`
  font-weigt: bold;
  color: green;
`;

const SideBarPost = styled.p`
  background: ${props => (props.active ? '#acacac' : '#fff')}
`;

export default function SideBar(props) {
    return (
    <div>
      {props.fetchingPosts &&
        <FetchingPosts>
          <FormattedMessage
            id="fetching-posts"
            defaultMessage="Fetching posts..."
            description="Fetching messages -text"
          />
        </FetchingPosts>}
      {props.posts.map(post => (
        <SideBarPost
          active={props.selectedPost && props.selectedPost.id === post.id}
          onClick={() => props.selectPost(post)}
          key={post.id}
        >
          {post.title.rendered}
        </SideBarPost>
      ))}
    </div>
  );
}
