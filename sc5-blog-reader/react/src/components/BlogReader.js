import React, { Component } from "react";
import styled from "styled-components";

import TechTitle from "./TechTitle";
import SideBar from "./SideBar";
import Post from "./Post";

const Container = styled.div`
    display: flex;
`;

const sideBarWidth = 15;
const SideBarContainer = styled.div`
    flex: ${sideBarWidth};
`;
const postMargin = 5;
const PostContainer = styled.div`
    flex: ${100 - sideBarWidth - postMargin * 2};
    margin: 0 5%;
`;

export default class BlogReader extends Component {
  componentWillMount() {
    console.log("Blogreader props", this.props);
    this.props.fetchPosts();
  }

  render() {
    return (
      <div>
        <TechTitle>{this.props.title}</TechTitle>
        <Container>
          <SideBarContainer><SideBar {...this.props} /></SideBarContainer>
          <PostContainer><Post post={this.props.selectedPost} /></PostContainer>
        </Container>
      </div>
    );
  }
}
