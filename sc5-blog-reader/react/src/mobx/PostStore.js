import { observable } from "mobx";
import api from "../utils/api";

export default class PostStore {
  @observable id;
  @observable fetchingPosts = false;
  @observable posts = [];
  @observable selectedPost = null;

  constructor() {
    this.id = new Date();

    this.fetchPosts = () => {
      this.fetchingPosts = true;  // this works but not shonw in the UI
      api.fetchPosts().then(posts => {
        this.posts = posts;
        this.fetchingPosts = false;
      });
    };

    this.selectPost = post => {
      this.selectedPost = post;
    };
  }

  // these won't pass on in props nor can they be used in inject
  // @action.bound;
  // fetchPosts() {
  //   this.fetchingPosts = true;
  //   api.fetchPosts().then(
  //     posts => {
  //       this.posts = posts;
  //       this.fetchingPosts = false;
  //     }
  //   )
  // }
}
