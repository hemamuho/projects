import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { observer } from "mobx-react";

import PostStore from "./PostStore";

import SideBar from "../components/SideBar";
import Post from "../components/Post";
import TechTitle from "../components/TechTitle";

@observer class Container extends Component {
  componentWillMount() {
    this.props.state.fetchPosts();
  }

  render() {
    return (
      <div>
        <SideBar
          posts={this.props.state.posts}
          selectedPost={this.props.state.selectedPost}
          selectPost={this.props.state.selectPost}
          fetchingPosts={this.props.fetchingPosts}
        />
        <Post post={this.props.state.selectedPost} />
      </div>
    );
  }
}

const appState = new PostStore();
class MobxApp extends Component {
  render() {
    return (
      <div>
        <TechTitle>
          <FormattedMessage
            id="mobx"
            defaultMessage="Mobx"
            description="Mobx title"
          />
        </TechTitle>
        <Container state={appState} />
      </div>
    );
  }
}

export default MobxApp;
