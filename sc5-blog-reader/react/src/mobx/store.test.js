import PostStore from "./PostStore";
const store = new PostStore();

// use the mock api in PostStore in this testcd 
jest.mock("../utils/api");

describe("poststore", () => {
  it("initialized with defaults", () => {
    expect(store.posts.length).toEqual(0);
    expect(store.fetchingPosts).toEqual(false);
  });

  it("fetches posts", () => {
    // one does not simply test async actions?
  });
});
