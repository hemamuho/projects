import React from "react";
import { provideState, injectState, softUpdate } from "freactal";
import { FormattedMessage } from "react-intl";

import BlogReader from "../components/BlogReader";

import api from "../utils/api";

export const wrapWithState = provideState({
  initialState: () => ({
    posts: [],
    fetchingPosts: false,
    selectedPost: null
  }),
  effects: {
    setFetchingPosts: (effects, val) => state => {
      return Object.assign({}, state, { fetchingPosts: val });
    },
    fetchPosts: effects =>
      effects
        .setFetchingPosts(true)
        .then(() => api.fetchPosts())
        .then(posts => state =>
          Object.assign({}, state, { posts, fetchingPosts: false })),
    selectPost: softUpdate((state, post) =>
      Object.assign({}, state, { selectedPost: post })
    )
  }
});

export const FreactalApp = wrapWithState(
  injectState(({ state, effects }) => (
    <BlogReader
      title={
        <FormattedMessage
          id="freactal"
          defaultMessage="Freactal"
          description="Freactal-title"
        />
      }
      {...state}
      {...effects}
    />
  ))
);

export default FreactalApp;
