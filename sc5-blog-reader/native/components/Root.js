import React from "react";
import {StackNavigator} from 'react-navigation'

import Main from "./Main";
import Post from "./Post";

const Root = StackNavigator({
    Main: {screen: Main},
    Post: {screen: Post}
})
export default Root;
