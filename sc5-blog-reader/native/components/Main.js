import React from "react";
import { View, Text, StyleSheet } from "react-native";
import ListItem from "./ListItem";

export default class Main extends React.Component {
  static navigationOptions = {
    title: "Posts"
  };

  constructor() {
    super();
    this.state = {
      posts: []
    };
  }

  componentWillMount() {
    fetch(`https://sc5.io/wp-json/wp/v2/posts`)
      .then(response => {
        if (response.status === 200) {
          return response.json();
        } else console.log("Error fetching");
      })
      .then(posts => this.setState({ posts }));
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.list}>
          {this.state.posts.map(post => (
            <ListItem
              key={post.id}
              text={post.title.rendered}
              deletePost={() => {
                  this.setState({
                      posts: this.state.posts.filter(p => p.id !== post.id)
                  })
              }}
              goToPost={() => {
                this.props.navigation.navigate("Post", { post: post });
              }}
            />
          ))}
        </View>
        <Text style={{textAlign: 'center', fontSize: 16, margin: 20}}>Drag items to sides to reveal functionality</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "stretch",
    justifyContent: "center"
  },
  list: {
    flex: 1,
    alignItems: "center"
  }
});
