import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Animated,
  TouchableHighlight
} from "react-native";
import Interactable from "react-native-interactable";

const animX = new Animated.Value(0);
const HIDDEN_WIDTH = 100;

const ListItem = props => {
  return (
    <View style={[styles.itemBG, styles.fill]}>
      <View style={[styles.hiddenButton, styles.delete, styles.fill]}>
        <TouchableHighlight onPress={() => props.deletePost()}>
          <Text style={{ color: "#fff" }}>Delete</Text>
        </TouchableHighlight>
      </View>
      <View style={[styles.hiddenButton, styles.read, styles.fill]}>
        <TouchableHighlight
          style={[styles.hiddenButton, styles.read, styles.fill]}
          onPress={() => props.goToPost()}
        >
          <Text style={{ color: "#fff" }}>Read</Text>
        </TouchableHighlight>
      </View>
      <Interactable.View
        horizontalOnly={true}
        boundaries={{
          left: -HIDDEN_WIDTH,
          right: HIDDEN_WIDTH
        }}
        snapPoints={[
          { x: -HIDDEN_WIDTH, damping: 0.5, tension: 300 },
          { x: 0, damping: 0.5, tension: 300 },
          { x: HIDDEN_WIDTH, damping: 0.5, tension: 300 }
        ]}
        style={styles.fill}
      >
        <View style={[styles.item, styles.fill]}>
          <Text>{props.text}</Text>
        </View>
      </Interactable.View>
    </View>
  );
};

const styles = StyleSheet.create({
  fill: {
    alignSelf: "stretch",
    alignItems: "center"
  },
  item: {
    padding: 10,
    borderBottomWidth: 1,
    borderColor: "#acacac",
    backgroundColor: "#fff"
  },
  itemBG: {
    backgroundColor: "#333"
  },
  hiddenButton: {
    position: "absolute",
    top: 0, bottom: 0,
    padding: 10,
    width: HIDDEN_WIDTH,
    alignItems: "center",
    justifyContent: 'center'
  },
  delete: {
    left: 0,
    backgroundColor: "#d87575"
  },
  read: {
    right: 0,
    backgroundColor: "#8cd875"
  }
});

export default ListItem;
