import React from "react";
import { WebView } from "react-native";

export default class Post extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.state.params.post.title.rendered
  });

  render() {
    const post = this.props.navigation.state.params.post;
    const html = `
        <h1>I'm a webview!</h1>
          <p>${post.content.rendered}</p>
          `;
    console.log("HTML:", html);
    return (
      <WebView
        scalesPageToFit={true}
        source={{
          html
        }}
      />
    );
  }
}
