import React from 'react';
import { StyleSheet, Text, View, Image, Animated } from 'react-native';

import Interactable from 'react-native-interactable';

export default class App extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      _deltaY: new Animated.Value(0),
      _deltaX: new Animated.Value(0)
    };
  }

  render() {
    const tiedToDeltaY = this.state._deltaY.interpolate({
      inputRange: [-100 ,100],
      outputRange: [50,100]
    });
    const tiedToDeltaX = this.state._deltaX.interpolate({
      inputRange: [-100 ,100],
      outputRange: [0,1]
    })
              

    return (
      <View style={styles.container}>
        <Interactable.View
          boundaries={{top: -100, bottom: 100, left: -100 , right: 100, bounce: 0.8}}
          animatedValueY={this.state._deltaY}
          animatedValueX={this.state._deltaX}
        >
          <Animated.View
            style={{width: 100, height: 100, alignSelf: 'center',
              opacity: tiedToDeltaX
            }}
          ><Animated.Image source={require('./images/sc5.png')} style={{alignSelf: 'center',
            height: tiedToDeltaY, 
            width: tiedToDeltaY
          }}/>
          </Animated.View>
        </Interactable.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
